import 'package:flutter/material.dart';

class ListWidget extends StatefulWidget {
  const ListWidget({super.key});

  @override
  State<ListWidget> createState() => _ListWidgetState();
}

class _ListWidgetState extends State<ListWidget> {
  var list = <Widget>[];
  add() {
    for (var i = 1; i <= 100; i++) {
      list.add(ListItemWidget(
        count: 0,
      ));
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    add();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView.builder(
        itemCount: list.length,
        itemBuilder: (context, index) => list[index],
      ),
    );
  }
}

class ListItemWidget extends StatefulWidget {
  int count;
  ListItemWidget({super.key, required this.count});
  @override
  State<ListItemWidget> createState() => _ListItemWidgetState();
}
class _ListItemWidgetState extends State<ListItemWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.all(4),
        child: Row(
          children: [
            Text(widget.count.toString()),
            MaterialButton(
              onPressed: () {
                setState(() {
                  widget.count++;
                });
              },
              child: const Text("+"),
            )
          ],
        ));
  }
}
